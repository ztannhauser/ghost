#! /usr/bin/env python

import sys;
import subprocess;
import os, copy;
import os.path;
import shelve;
from os.path import isfile, isdir, join;

def call(command):
	print(command);
	p = subprocess.Popen(command);
	p_status = p.wait();
	if(p_status):
		exit(p_status);

def install(package):
	print("# Installing " + package);
	if(not isdir(package)):
		print("# Download repo:");
		url = "https://gitlab.com/ztannhauser/" + package + ".git";
		print("url == " + url);
		call(["git", "clone", url]);
	print("# Reading dependencies: ");
	makefile = open(join(package, "makefile"), "r");
	lines = map(lambda line: line[:-1], makefile.readlines());
	makefile.close();
	beginning = "deps += ../"
	lines = [line for line in lines if line.startswith(beginning)];
	subpackages = map(lambda line: line[len(beginning):], lines);
	print("# Installing Dependencies for " + package + ": ");
	print(subpackages);
	for subpackage in subpackages:
		install(subpackage);
	print("# Installing " + package + ":");
	call([
		"make",
		"-C",
		package,
		"install"
	]);
	print("# Done installing " + package);

def help():
	print("help");

args = sys.argv;
n_args = len(args);

if(n_args > 1):
	i_args = 1;
#	call(["mkdir", "-p", "git"]);
#	os.chdir("git");
	while(i_args < n_args):
		arg = args[i_args];
		i_args = i_args + 1;
		install(arg);
else:
	print("Error: No commands given");
	help();





